package com.mamezou.tools;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.safaribooks.junitattachments.CaptureFile;
import com.safaribooks.junitattachments.RecordAttachmentRule;

/**
 * 画面表示と画面遷移を伴うテストの基底クラス。
 */
public class WebPageScenario {

	/* 定数 */

	/** エビデンス(画像)を出すディレクトリ。 */
	private static final File EVIDENCE_DIR = new File(
			"target/test-evidence/screenshot");

	/** WebDriver */
	protected WebDriver driver = null;
	
	@Rule
	public RecordAttachmentRule recordArtifactRule = new RecordAttachmentRule(this);

	@CaptureFile(extension = "png")
	public byte[] capturePage = null;
	
	/* テスト全体の事情と道具 */

	/** メソッド名検出用のルール。 */
	@Rule
	public TestName names = new TestName();

	/** テスト事前処理。 */
	@Before
	public void setUp() throws MalformedURLException {
		// エビデンスのフォルダ作成。
		File methodEvidenceDir = getTestMethodEvidenceDir();
		if (!methodEvidenceDir.exists()) {
			methodEvidenceDir.mkdirs();
		}
		// ドライバの取得。
		driver = new FirefoxDriver();
		// シナリオテストでの明示的なタイムアウト時間を設定(10秒)
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	/** テスト事後処理。 */
	@After
	public void tearDown() {
		if (driver != null) {
			// メソッド終わりでとりあえずスクショ取る。
//			getScreanshot("testMethodEnd");

			// capture a screenshot
			if (driver instanceof TakesScreenshot) {
				capturePage = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.BYTES);
			}

			// Driver殺す。
			driver.close();
			driver = null;
		}
	}

	/**
	 * テスト中ブラウザの画面のスクリーンショット画像を取る。 引数に指定した文字で "[文字].png" というPNG画像ファイルが作成される。
	 *
	 * @param themeName
	 *            テーマ名。(画像ファイル名になる)
	 */
	protected void getScreanshot(String themeName) {
		try {
			File methodEvidenceDir = getTestMethodEvidenceDir();
			if (methodEvidenceDir.exists()) {
				File scrFile = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileHandler.copy(scrFile, new File(methodEvidenceDir, themeName
						+ ".png"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 画面スクリーンショットを保存する「テストメソッド毎のエビデンスディレクトリ｣を返す。
	 *
	 * @return テストメソッド毎のエビデンスのディレクトリのFileオブジェクト。
	 */
	protected File getTestMethodEvidenceDir() {
		return new File(EVIDENCE_DIR, this.getClass().getName() + "/"
				+ names.getMethodName());
	}

	/* ヘルパーメソッド。 */

	/**
	 * WebDriver.findElement()をタイムアウト付きで実行するエイリアス。
	 *
	 * @param driver
	 *            WebDriverオブジェクト。
	 * @param by
	 *            Byオブジェクト。
	 * @param timeoutSeccond
	 *            タイムアウト時間(秒)。
	 * @return WebDriver.findElement() の結果。
	 */
	public WebElement findElementWithTO(WebDriver driver, final By by,
			int timeoutSeccond) {
		return (new WebDriverWait(driver, 10))
				.until(new ExpectedCondition<WebElement>() {
					public WebElement apply(WebDriver d) {
						return d.findElement(by);
					}
				});
	}
}