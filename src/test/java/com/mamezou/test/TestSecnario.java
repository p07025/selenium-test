package com.mamezou.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mamezou.tools.WebPageScenario;

public class TestSecnario extends WebPageScenario {

	@Test
	public void testImage() throws Exception {
		driver.get("http://www.google.co.jp");
		Wait<WebDriver> wait = new WebDriverWait(driver, 30);

		WebElement inputField = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//input[@type='text']")));
		assertEquals(driver.getTitle(), "Google");
		inputField.sendKeys("初音ミク");
		inputField.sendKeys(Keys.ENTER);
		wait.until(ExpectedConditions.titleContains("初音ミク"));

		WebElement imageButton = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By
						.xpath("//a[contains(@href,'tbm=isch')]")));
		imageButton.sendKeys(Keys.ENTER);
	}

}
